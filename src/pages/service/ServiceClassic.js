import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import PageContainer from '../../container/page';
import Header from '../../components/header/HeaderTwo';
import PopupSearch from '../../components/popup/PopupSearch';
import PopupMobileMenu from '../../components/popup/PopupMobileMenu';
import Breadcrumb from '../../components/breadcrumb/BreadcrumbSix';
import Services from '../../container/service/ServiceEight';
import Works from '../../container/work/WorkFour';
import CallToAction from '../../container/call-to-action/pages/CallToActionOne';
import FooterOne from '../../container/footer/FooterOne';

const ServiceClassic = () => {
  return (
    <Fragment>
      <Helmet>
        <title>LifeLine || Improving The Lives Of Many</title>
      </Helmet>

      <PageContainer
        classes='template-color-1 template-font-1'
        revealFooter={false}
      >
        <Header />
        <PopupSearch />
        <PopupMobileMenu />
        <Breadcrumb
          title='Tentang Kami'
          heading='LifeLine'
          text='Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, eligendi. Atque blanditiis tenetur accusantium porro odio laboriosam voluptatibus dignissimos nesciunt alias libero eaque, ea omnis quae ipsum repellendus eligendi magni. Ab sint perspiciatis animi, ut voluptate magnam voluptatibus impedit, facilis asperiores doloribus suscipit. Maiores nesciunt tempora, ipsum suscipit aliquid assumenda!'
        />
        {/* <Services /> */}
        <Works />
        <CallToAction bgColor={'#0038E3'} />
        <FooterOne />
      </PageContainer>
    </Fragment>
  );
};

export default ServiceClassic;
